FROM ubuntu

# Update system
RUN apt update
RUN apt full-upgrade -y

# Install fetchmail
RUN apt install fetchmail spamassassin -y